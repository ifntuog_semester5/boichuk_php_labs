<?php
function task_one($x, $a, $b) {
    $y = 0;
    if($x == $a){
        $y = exp(abs($x+$a))*sin($x);
    }
    else if($a < $x and $a**2 > $x){
        $y = ($x-$a)**2*cos($x)**2;
    }
    return $y;
}

function &task_two($a, $b, $start, $step, $n) {
    $x = $start;
    $res[$n] = array();
    for($i = 0; $i < $n; $i++) {
        $t = (pi()-$x**2)*sin(sqrt(2.1*$b+$x*log($a)));
        $res[$i] = $t;
        $x += $step;
    }
    return $res;
}

echo("Task one:\n");
echo(task_one(1.62, 1.62, -1.25) . "\n");
echo(task_one(1.41, 1.62, -1.25) . "\n");

echo("\n");

echo("Task two:\n");
$res =& task_two(21.4, 1.95, 4.6, 1.5, 8);
for($i = 0; $i < 8; $i++) {
    echo($res[$i] . "\n");
}
?>
