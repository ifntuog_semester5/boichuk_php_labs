<?php
//second task
$a = 21.4;
$b = 1.95;
$start = 4.6;
$step = 1.5;
$n = 8;
$x = $start;
echo("Task two:\n");
for($i = 0; $i < $n; $i++) {
    $t = (pi()-$x**2)*sin(sqrt(2.1*$b+$x*log($a)));
    echo($t . "\n");
    $x += $step;
}
?>